#pragma once
/*
 * This struct is used internally to represent
 * upperLeft and lowerRight coordinates.
 */
struct Point
{
	Point() : x(0), y(0) {}
	~Point() {}
	Point(int x, int y) : x(x), y(y) {}
	int x;
	int y;
};

