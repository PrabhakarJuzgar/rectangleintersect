IntersectingRectangles - 
Determine set of all intersecting rectangles. Each rectangle may be intersecting with one or more rectangles.

Getting started - 
Create a new folder into your local machine and download all the *.cpp and *.h files into it.

Prerequisites - 
Requires Microsoft Visual Studio 2017 Community Edition or above.
For JSON parsing, I have used -

https://github.com/nlohmann/json

This is already part of the project in the file json.h

Installing - 
Create a new console application project in Microsoft Visual Studio and add the downloaded files to the project.
Build the solution.

Usage - 
Once the build is complete, the .exe generated can be used from the command line as follows.

C:\Users\AnuPrabhakar\source\repos\RectangleIntersect>Debug\RectangleIntersect.exe foo1.json
Debug\RectangleIntersect.exe foo1.json

Input

1: Rectangle at (100,100), w=250, h=80.

2: Rectangle at (100,100), w=250, h=80.

3: Rectangle at (120,200), w=250, h=150.

4: Rectangle at (140,160), w=250, h=100.

5: Rectangle at (160,140), w=350, h=190.

Intersections

Between rectangle 1 and 2 at (100,100), w=250, h=80.

Between rectangle 1 2 and 4 at (140,160), w=210, h=20.

Between rectangle 1 2 4 and 5 at (160,160), w=190, h=20.

Between rectangle 1 2 and 5 at (160,140), w=190, h=40.

Between rectangle 1 and 4 at (140,160), w=210, h=20.

Between rectangle 1 4 and 5 at (160,160), w=190, h=20.

Between rectangle 1 and 5 at (160,140), w=190, h=40.

Between rectangle 2 and 4 at (140,160), w=210, h=20.

Between rectangle 2 4 and 5 at (160,160), w=190, h=20.

Between rectangle 2 and 5 at (160,140), w=190, h=40.

Between rectangle 3 and 4 at (140,200), w=230, h=60.

Between rectangle 3 4 and 5 at (160,200), w=210, h=60.

Between rectangle 3 and 5 at (160,200), w=210, h=130.

Between rectangle 4 and 5 at (160,160), w=230, h=100.


Author - 
Prabhakar Juzgar