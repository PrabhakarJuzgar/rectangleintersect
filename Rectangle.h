#pragma once
#include "Point.h"
class Rectangle
{
public:
	Rectangle() {};
	~Rectangle() {};
	Rectangle(int x, int y, int w, int h)
	{
		topLeft.x = x;
		topLeft.y = y;
		bottomRight.x = x + w;
		bottomRight.y = y + h;
		width = w;
		height = h;
	}
	void setLeftTop(Point &pnt)
	{
		topLeft.x = pnt.x;
		topLeft.y = pnt.y;
	}
	void setBottomRight(Point &pnt)
	{
		bottomRight.x = pnt.x;
		bottomRight.y = pnt.y;
	}
	Point getLeftTop() const { return topLeft; }
	Point getBottomRight() const { return bottomRight; }
	void setWidth(int w) { width = w; }
	void setHeight(int h) { height = h; }
	int getWidth() { return width; }
	int getHeight() { return height; }
private:
	Point topLeft;
	Point bottomRight;
	int width;
	int height;
};