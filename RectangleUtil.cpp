#include "pch.h"
#include "RectangleUtil.h"
#include <algorithm>
#include <vector>
#include <iostream>
#include <set>
#include <sstream>
/*
 * Comare class to sort Rectangles
 */
struct sortRect {
	bool operator()(const Rectangle &a, const Rectangle &b) const
	{
		return a.getLeftTop().x < b.getLeftTop().x;
	}
}srtRect;

RectangleUtil::RectangleUtil()
{
}

RectangleUtil::~RectangleUtil()
{
}

/*
 * This API returns true if two Rectangles are intersecting.
 */
bool RectangleUtil::areIntersecting(Rectangle & a, Rectangle & b)
{
	Point pointATop = a.getLeftTop();
	Point pointABottom = a.getBottomRight();
	Point pointBTop = b.getLeftTop();
	Point pointBBottom = b.getBottomRight();
	// x1+w1 < x2
	if ((pointABottom.x < pointBTop.x) ||
		// x2+w2 < x1
		(pointBBottom.x < pointATop.x) ||
		// y1+h1 < y2
		(pointABottom.y < pointBTop.y) ||
		// y2+h2 < y1
		(pointBBottom.y < pointATop.y)) {
		return false;
	}
	return true;
}

/*
 * This API returns coordinates of the intersecting Rectangle.
 */
void RectangleUtil::getIntersectingRect(Rectangle & a, Rectangle & b, Rectangle &intersectingRect)
{
	Point pntLeft, pntRight;
	pntLeft.x = std::max(a.getLeftTop().x, b.getLeftTop().x);
	pntLeft.y = std::max(a.getLeftTop().y, b.getLeftTop().y);

	pntRight.x = std::min(a.getBottomRight().x, b.getBottomRight().x);
	pntRight.y = std::min(a.getBottomRight().y, b.getBottomRight().y);
	int width = pntRight.x - pntLeft.x;
	int height = pntRight.y - pntLeft.y;

	//Rectangle rect(pntLeft.x, pntLeft.y, width, height);
	intersectingRect.setHeight(height);
	intersectingRect.setWidth(width);
	intersectingRect.setLeftTop(pntLeft);
	intersectingRect.setBottomRight(pntRight);

	return;
}

/*
 * This API recursively calls itself to check whether given
 * rectangles are intersecting.
 * The logic is similar to printing all subsequences of a given
 * string.
 */
void RectangleUtil::util(std::vector<Rectangle>& input, std::vector<std::pair<Rectangle,
	std::vector<Rectangle>>>& output, size_t idx, Rectangle &curRect, std::vector<Rectangle> &stk)
{
	if (idx == input.size()) return;
	for (size_t i = idx; i < input.size(); i++) {
		if (areIntersecting(curRect, input[i]))
		{
			// Make note the current Rectangle to be used later
			stk.push_back(curRect);
			numRect.push_back(i + 1);
			posOfIntersectingRects.push_back(numRect);
			Rectangle intersectingRect;
			getIntersectingRect(curRect, input[i], intersectingRect);
			if (dictRectangles.find(curRect) != dictRectangles.end())
			{
				std::vector<Rectangle> temp = dictRectangles[curRect];
				temp.push_back(input[i]);
				dictRectangles[intersectingRect] = temp;
				output.push_back(make_pair(intersectingRect, temp));
			}
			else
			{
				std::vector<Rectangle> temp{ curRect, input[i] };
				dictRectangles.insert(make_pair(intersectingRect, temp));
				output.push_back(make_pair(intersectingRect, temp));
			}
			util(input, output, i + 1, intersectingRect, stk);
			// backtrack
			curRect = stk.back();
			stk.pop_back();
			numRect.pop_back();
		}
	}
	dictRectangles.clear();
}

/*
 * This API is invoked by the consumer to print all intersecting Rectangles
 */
void RectangleUtil::numOfIntersectingRects(std::vector<Rectangle>& input)
{
	std::vector < std::pair<Rectangle, std::vector<Rectangle>>> output;

	std::sort(input.begin(), input.end(), srtRect);
	std::vector<Rectangle> stk;
	for (size_t i = 1; i < input.size(); i++)
	{
		numRect.push_back(i);
		util(input, output, i, input[i - 1], stk);
		dictRectangles.clear();
		numRect.clear();
	}
	std::cout << "Input" << std::endl;
	for (size_t i = 0; i < input.size(); i++) {
		std::ostringstream oss;
		oss << i + 1 << ": Rectangle at (";
		oss << input[i].getLeftTop().x << "," << input[i].getLeftTop().y << "), w=";
		oss << input[i].getWidth() << ", h=" << input[i].getHeight() << ".";
		std::cout << oss.str() << std::endl;
	}
	std::cout << "Intersections" << std::endl;
	if (output.empty())
	{
		std::cout << "There are no intersections between given Rectangles" << std::endl;
		return;
	}
	for (size_t i = 0; i < output.size(); i++) {
		std::ostringstream oss;
		oss << "Between rectangle ";
		for (size_t j = 0; j < posOfIntersectingRects[i].size(); j++)
		{
			oss << posOfIntersectingRects[i][j] << " ";
			if (j == posOfIntersectingRects[i].size() - 2)
			{
				oss << "and ";
			}
		}
		oss << "at (";
		oss << output[i].first.getLeftTop().x << "," << output[i].first.getLeftTop().y << "), w=";
		oss << output[i].first.getWidth() << ", h=" << output[i].first.getHeight() << ".";
		std::cout << oss.str() << std::endl;
	}
	std::cout << std::endl;
}
