#pragma once
#include "Rectangle.h"
#include <vector>
#include <map>
#include <utility>

/*
 * This class is used to sort entries getting
 * stored in the std::map - dictRectangles
 */
struct compareRect {
	bool operator()(const Rectangle &a, const Rectangle &b) const
	{
		return a.getLeftTop().x < b.getLeftTop().x;
	}
};
class RectangleUtil
{
private:
	/*
	 * This will store all the rectangles intersecting with a
	 * given rectangle.
	 */
	std::map<Rectangle, std::vector<Rectangle>, compareRect> dictRectangles;
	/*
	 * This vector is used to mark all the rectangles intersecting
	 * with a given rectangle at a given cycle
	 */
	std::vector<int> numRect;
	/*
	 * Positions of all the intersecting rectangles.
	 */
	std::vector<std::vector<int>> posOfIntersectingRects;
public:
	RectangleUtil();
	~RectangleUtil();
	bool areIntersecting(Rectangle &a, Rectangle &b);
	void getIntersectingRect(Rectangle &a, Rectangle &b, Rectangle &intersetingRectangle);
	void util(std::vector< Rectangle > &v, std::vector < std::pair<Rectangle,
		std::vector< Rectangle >>> &vv, size_t idx, Rectangle &curRect, std::vector<Rectangle> &stk);
	void numOfIntersectingRects(std::vector<Rectangle>& v);
};

