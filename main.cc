// main.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <exception>
#include "Rectangle.h"
#include "RectangleUtil.h"
#include "json.h"

using json = nlohmann::json;

int main(int argc, char *argv[])
{
	if (argc != 2)
	{
		std::cout << "Uasge: " << std::endl;
		std::cout << "<Name.exe> <JSON filepath>" << std::endl;
		return 0;
	}
	for (int i = 0; i < argc; i++) {
		std::cout << argv[i] << " ";
	}
	std::string fileName = argv[1];
	std::ifstream ifs(fileName);
	json j;
	try
	{
		ifs >> j;
	}
	catch (std::exception &ex)
	{
		std::cout << ex.what() << std::endl;
		return 0;
	}
	catch (...)
	{
		std::cout << "Something went wrong while reading the file " << fileName << std::endl;
		return 0;
	}

	j = j["rects"];
	std::vector<Rectangle> vecRects;
	for (json::iterator it = j.begin(); it != j.end(); ++it)
	{
		std::vector<int> vecCoordinates;
		if (it->find("x") != it->end()) {
			vecCoordinates.push_back(*it->find("x"));
		}
		if (it->find("y") != it->end()) {
			vecCoordinates.push_back(*it->find("y"));
		}
		if (it->find("w") != it->end()) {
			vecCoordinates.push_back(*it->find("w"));
		}
		if (it->find("h") != it->end()) {
			vecCoordinates.push_back(*it->find("h"));
		}
		Rectangle rect(vecCoordinates[0], vecCoordinates[1], vecCoordinates[2], vecCoordinates[3]);
		vecRects.push_back(rect);
	}
	RectangleUtil rectUtilFromFile;
	rectUtilFromFile.numOfIntersectingRects(vecRects);
#if 0
	std::vector<Rectangle> v;
	Rectangle rect1(100, 100, 250, 80);
	v.push_back(rect1);
	Rectangle rect2(120, 200, 250, 150);
	v.push_back(rect2);
	Rectangle rect3(140, 160, 250, 100);
	v.push_back(rect3);
	Rectangle rect4(160, 140, 350, 190);
	v.push_back(rect4);
	RectangleUtil rectUtil;
	rectUtil.numOfIntersectingRects(v);

	v.clear();
	Rectangle rect5(100, 100, 250, 250);
	v.push_back(rect5);
	Rectangle rect6(80, 80, 230, 230);
	v.push_back(rect6);
	Rectangle rect7(60, 60, 210, 210);
	v.push_back(rect7);
	Rectangle rect8(40, 40, 190, 190);
	v.push_back(rect8);
	RectangleUtil rectUtil1;
	rectUtil1.numOfIntersectingRects(v);

	v.clear();
	Rectangle rect9(100, 100, 250, 250);
	v.push_back(rect9);
	Rectangle rect10(80, 80, 230, 230);
	v.push_back(rect10);
	Rectangle rect11(60, 60, 210, 210);
	v.push_back(rect11);
	Rectangle rect12(40, 40, 190, 190);
	v.push_back(rect12);
	Rectangle rect13(100, 100, 250, 250);
	v.push_back(rect13);
	RectangleUtil rectUtil2;
	rectUtil2.numOfIntersectingRects(v);

	v.clear();
	Rectangle rect14(100, 100, 250, 250);
	v.push_back(rect14);
	Rectangle rect15(80, 80, 230, 230);
	v.push_back(rect15);
	Rectangle rect16(60, 60, 210, 210);
	v.push_back(rect16);
	Rectangle rect17(40, 40, 190, 190);
	v.push_back(rect17);
	Rectangle rect18(100, 100, 250, 250);
	v.push_back(rect18);
	Rectangle rect19(40, 40, 190, 190);
	v.push_back(rect19);
	RectangleUtil rectUtil3;
	rectUtil3.numOfIntersectingRects(v);
#endif
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
